Pacman Multi-agent assignment from UC Berkeley, found at http://ai.berkeley.edu/multiagent.html.

This code implements question 1 till 5. The AI is very competent in completing a game of Pacman with a good success rate. Completely written in Python.