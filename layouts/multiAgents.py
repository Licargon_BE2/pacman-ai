# multiAgents.py
# --------------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
  """
    A reflex agent chooses an action at each choice point by examining
    its alternatives via a state evaluation function.

    The code below is provided as a guide.  You are welcome to change
    it in any way you see fit, so long as you don't touch our method
    headers.
  """


  def getAction(self, gameState):
    """
    You do not need to change this method, but you're welcome to.

    getAction chooses among the best options according to the evaluation function.

    Just like in the previous project, getAction takes a GameState and returns
    some Directions.X for some X in the set {North, South, West, East, Stop}
    """
    # Collect legal moves and successor states
    legalMoves = gameState.getLegalActions()

    # Choose one of the best actions
    scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
    bestScore = max(scores)
    bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
    chosenIndex = random.choice(bestIndices) # Pick randomly among the best

    "Add more of your code here if you want to"

    return legalMoves[chosenIndex]

  def evaluationFunction(self, currentGameState, action):
    """
    Design a better evaluation function here.

    The evaluation function takes in the current and proposed successor
    GameStates (pacman.py) and returns a number, where higher numbers are better.

    The code below extracts some useful information from the state, like the
    remaining food (oldFood) and Pacman position after moving (newPos).
    newScaredTimes holds the number of moves that each ghost will remain
    scared because of Pacman having eaten a power pellet.

    Print out these variables to see what you're getting, then combine them
    to create a masterful evaluation function.
    """
    # Useful information you can extract from a GameState (pacman.py)
    successorGameState = currentGameState.generatePacmanSuccessor(action)
    newPos = successorGameState.getPacmanPosition()
    oldFood = currentGameState.getFood()
    foodList = oldFood.asList()
    newGhostStates = successorGameState.getGhostStates()
    newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

    "*** YOUR CODE HERE ***"
    returnValue = 0;
	
    distToClosestGhost = oldFood.width + oldFood.height # Standard the dimensions of the grid, AKA max distance something can be away from us
    for ghost in newGhostStates:
        if (ghost.scaredTimer <= 0): # If scared: harmless
		    	distToClosestGhost = min( manhattanDistance(newPos, ghost.getPosition()), distToClosestGhost)
			
    if distToClosestGhost < 2: # Ghost too close: Bad situation: Avoid this at all costs
		  returnValue = -500
	
    distToClosestFood = oldFood.width + oldFood.height
    for food in foodList:
      distToClosestFood = min(manhattanDistance(newPos, food), distToClosestFood)

    if (distToClosestFood > 0 ): # Something I get 0 distance to a food, so I need to avoid this
      inverseDistance = 1.0/distToClosestFood # The further away the closest food is, the worse the situation is: Avoid this at all costs
    else:
      inverseDistance = 1
   
    returnValue += inverseDistance
    if oldFood[newPos[0]][newPos[1]]: # If there is a food at the new position: Woot!
      returnValue += 5

    return returnValue

def scoreEvaluationFunction(currentGameState):
  """
    This default evaluation function just returns the score of the state.
    The score is the same one displayed in the Pacman GUI.

    This evaluation function is meant for use with adversarial search agents
    (not reflex agents).
  """
  return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
  """
    This class provides some common elements to all of your
    multi-agent searchers.  Any methods defined here will be available
    to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

    You *do not* need to make any changes here, but you can if you want to
    add functionality to all your adversarial search agents.  Please do not
    remove anything, however.

    Note: this is an abstract class: one that should not be instantiated.  It's
    only partially specified, and designed to be extended.  Agent (game.py)
    is another abstract class.
  """

  def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
    self.index = 0 # Pacman is always agent index 0
    self.evaluationFunction = util.lookup(evalFn, globals())
    self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
  """
    Your minimax agent (question 2)
  """
  from util import *
  
  def GhostMinEval(self, gameState, gIndex, currentDepth):
    if (gameState.isLose() or gameState.isWin() or currentDepth == self.depth): # We've reached a leaf: evaluate.
        return self.evaluationFunction(gameState)
    ghosts = gameState.getNumAgents()-1;
	
    allowedActions = gameState.getLegalActions(gIndex)
    results = list()
    for action in allowedActions:
      results.append( gameState.generateSuccessor(gIndex, action))
    scores = list()
    if (gIndex == ghosts): # We did all the ghosts: go back to pacman max eval
      scores = [self.PacmanMaxEval(result, currentDepth +1) for result in results]
    elif (gIndex < ghosts): # There are still ghosts left: treat the next one recursively
      scores = [self.GhostMinEval(result, gIndex+1, currentDepth) for result in results]

    return min(scores) # Return the minimum of the scores
      
  def PacmanMaxEval(self, gameState, currentDepth):
    if (gameState.isLose() or gameState.isWin() or currentDepth == self.depth): # We've reached a leaf: evaluate.
      return self.evaluationFunction(gameState)
    allowedActions = gameState.getLegalPacmanActions()
    results = list() 
    for action in allowedActions:
      if not action == Directions.STOP:
        results.append(gameState.generatePacmanSuccessor(action))
    scores = list()
    scores = [self.GhostMinEval(result, 1, currentDepth) for result in results] # Get the ghostscores, return the maximum
    return max(scores)
          
  def getAction(self, gameState):

    allowedActions = gameState.getLegalPacmanActions()
    results = list()
    allowedActions.remove(Directions.STOP) # Remove stop direction to speed up calculations
    for action in allowedActions:
      results.append(gameState.generatePacmanSuccessor(action))
    scores = list()
    scores = [self.GhostMinEval(result, 1, 0) for result in results]
    bestScore = max(scores) # Best score is the highest one (AKA best move for MAX, pacman)
    bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
    chosenIndex = random.choice(bestIndices) # Pick randomly among the best
    return allowedActions[chosenIndex]

class AlphaBetaAgent(MultiAgentSearchAgent):
  """
    Your minimax agent with alpha-beta pruning (question 3)
  """

  def getAction(self, gameState):
    """
      Returns the minimax action using self.depth and self.evaluationFunction
    """
    "*** YOUR CODE HERE ***"
    util.raiseNotDefined()

class ExpectimaxAgent(MultiAgentSearchAgent):
  """
    Your expectimax agent (question 4)
  """

  def getAction(self, gameState):
    """
      Returns the expectimax action using self.depth and self.evaluationFunction

      All ghosts should be modeled as choosing uniformly at random from their
      legal moves.
    """
    "*** YOUR CODE HERE ***"
    util.raiseNotDefined()

def betterEvaluationFunction(currentGameState):
  """
    Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
    evaluation function (question 5).

    DESCRIPTION: <write something here so we know what you did>
  """
  "*** YOUR CODE HERE ***"
  util.raiseNotDefined()

# Abbreviation
better = betterEvaluationFunction

class ContestAgent(MultiAgentSearchAgent):
  """
    Your agent for the mini-contest
  """

  def getAction(self, gameState):
    """
      Returns an action.  You can use any method you want and search to any depth you want.
      Just remember that the mini-contest is timed, so you have to trade off speed and computation.

      Ghosts don't behave randomly anymore, but they aren't perfect either -- they'll usually
      just make a beeline straight towards Pacman (or away from him if they're scared!)
    """
    "*** YOUR CODE HERE ***"
    util.raiseNotDefined()

